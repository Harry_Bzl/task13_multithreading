package exemples;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class VolatileTest {
    private volatile static long A=0;

    class MyThread extends Thread {
        @Override
        public void run() {
            for (int i=1; i<100000000; i++){
                A++;
            }
            System.out.println("finish" + Thread.currentThread().getName());
        }
    }

    public void show() {
        MyThread t1 = new MyThread();
        MyThread t2 = new MyThread();
        MyThread t3 = new MyThread();
        System.out.println(LocalDateTime.now());
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        }
        catch (InterruptedException e){

        }
        System.out.println(LocalDateTime.now());
        System.out.println("A=" + A);
    }

    public static void main(String[] args) {
        VolatileTest vt = new VolatileTest();
        vt.show();
    }
}
