package exemples;

public class InterruptTest {
    public static void main(String[] args) {
        Thread myThread = new Thread(
                ()->{
                    for (int i = 1; i<=10;i++){
                        try {
                            System.out.println("sec=" + i);
                            Thread.sleep(1000);
                        }
                        catch (InterruptedException e){
                            System.out.println("Sleeping thread interrupted"); break;
                        }
                    }
                }
        );
        myThread.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e){
            System.out.println("Main sleeping thread interrupted");
        }
        myThread.interrupt();
        System.out.println("Finish");
    }
}
