package tasks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciExecutors {
    public static void main(String[] args) {
        ExecutorService es= Executors.newFixedThreadPool(3);
        es.submit(()->
                new Fibonacci(100, "First").getFibonacciNumbers());
        es.submit(()->
                new Fibonacci(50,"Second").getFibonacciNumbers());
        es.submit(()->
                new Fibonacci(80,"Third").getFibonacciNumbers());
        es.shutdown();
    }
}
