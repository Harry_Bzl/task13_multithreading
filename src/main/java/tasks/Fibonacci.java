package tasks;

import java.util.stream.IntStream;

public class Fibonacci {
    private int size;
    private String name;
    public Fibonacci (int size, String name){
        this.size = size;
        this.name = name;
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(()->new Fibonacci(100,"First").getFibonacciNumbers());
        Thread t2 = new Thread(()->new Fibonacci(50, "Second").getFibonacciNumbers());
        Thread t3 = new Thread(()->new Fibonacci(80, "Third").getFibonacciNumbers());
        t1.start();
        t2.start();
        t3.start();
    }
    public void getFibonacciNumbers () {
        int first = 0;
        int second = 1;
        for (int i=0; i<size; i++){
            int numFibonacci = first+second;
            System.out.println(name + " " + numFibonacci);
            first = second;
            second=numFibonacci;
        }
    }
}
