package tasks;

public class PingPong {
    private static Object syn = new Object();
    public static void main(String[] args) {
        PingPong pn = new PingPong();
        pn.show();
    }

    public void show (){
        Thread t1 = new Thread(()->{synchronized (syn){
            for (int i=1; i<=5; i++){
                try{syn.wait();
                    Thread.sleep(1000);
                }
                catch (InterruptedException e){}
                System.out.println("Ping");

                syn.notify();
            }
        }});
        Thread t2 = new Thread(()->{synchronized (syn){
            for (int i=1; i<=5; i++){
                syn.notify();
                try{syn.wait();
                    Thread.sleep(1000);
                }
                catch (InterruptedException e){}
                System.out.println("Pong");

            }
        }});
        t1.start();
        t2.start();
    }

}
